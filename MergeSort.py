import os
import random
import sys


def sort(seq):

	def merge(left, right):

		result = []
		n, m = 0, 0

		while n < len(left) and m < len(right):

			if left[n] <= right[m]:
				result.append(left[n])
				n += 1
			else:
				result.append(right[m])
				m += 1

		result += left[n:]
		result += right[m:]

		return result

	if isinstance(seq, list):

		if len(seq) in (0, 1):
			return seq
		else:
			middle = len(seq) // 2
			left = sort(seq[:middle])
			right = sort(seq[middle:])

			return merge(left, right)


# ---------------------------------- #

try:
	# Example of use
	arr = []

	# Generous 10 random number
	for _ in range(10):
		arr.append(random.randint(-50, 50))

	# Displays the table before sorting
	sys.stdout.write(f"Before : {arr}\n")

	# Displays the table after sorting
	sys.stdout.write(f"After  : {sort(arr)}\n")
except:
	pass

if sys.platform.startswith('win32'):
	os.system("pause")